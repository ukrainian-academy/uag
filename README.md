# UAG

UAG is an image/video generator for Ukrainian letters and words.

## Usage

Create `input` folder structure, for instance:

```
input
├── а
│   ├── ананас
│   │   ├── ananas.mp3
│   │   └── ananas.jpg
…
```

and then type `python -m uag.uag` to generate images and videos under the `output` folder.

The `cache` folder is maintained to remember what was generated previously to skip it on each new generation for shorter generation time; this folder can be safely removed.

## Dependencies

* Python libs:
  * `tqdm` for progress bar
  * `wand` for image creation
  * `wcmatch` for file globbing
  * `xxhash` for caching
* apps:
  * `ffmpeg` for video generation

## Installation

`python-flit` is used as a build backend.

To build a wheel, install `python-build` and run:

```
$ python -m build
```

To install a wheel, make sure `python-installer` is available, and then run:

```
$ python -m installer --destdir=/some/dir dist/uag-…-.whl
```

This will also generate a startup Python script under `<destdir>/usr/bin` named `uag` which should be used instead of invoking `python -m uag.uag`.

Pre-built package is available for Arch Linux: [![build result](https://build.opensuse.org/projects/home:post-factum/packages/uag/badge.svg?type=default)](https://build.opensuse.org/package/show/home:post-factum/uag)

## Contributing

Feel free to mail us directly: [olha@cherevyk.name](mailto:olha@cherevyk.name), [oleksandr@natalenko.name](mailto:oleksandr@natalenko.name).

## Licensing

The code is licensed under terms and conditions of copyleft-next v0.3.1. See the `LICENSE` file for more details.
