#!/usr/bin/env python

# SPDX-License-Identifier: copyleft-next-0.3.1

from multiprocessing import Pool
from os import sched_getaffinity
from pathlib import Path
from subprocess import run
from sys import exit as sysexit
from textwrap import wrap
from typing import Optional, Tuple

from tqdm import tqdm
from wand.color import Color
from wand.drawing import Drawing
from wand.image import Image
from wcmatch.glob import BRACE, glob, iglob
from xxhash import xxh64

CONST_X = 1000
CONST_Y = 1000
CONST_START_Y = 750
CONST_START_FONT = 145
CONST_START_STROKE = 5
CONST_FILL_COLOR = "#3889f5"
CONST_FONT_PATH = "/usr/share/fonts/liberation/LiberationSans-Bold.ttf"


SPACING_PHRASE = 13
SPACING_LINE = 150
SPACING_FONT = 25
SPACING_STROKE = 1


SHIFT_Y = 75


STEP_Y = 25


def draw_image(input_image_file: str, phrase: str, output_image_file: str):
    text = wrap(f"{phrase}", SPACING_PHRASE)
    start_y = CONST_START_Y - SPACING_LINE * (len(text) - 1) + SHIFT_Y * (len(text) - 1)
    font_size = CONST_START_FONT - SPACING_FONT * (len(text) - 1)
    stroke_width = CONST_START_STROKE - SPACING_STROKE * (len(text) - 1)

    with Image(filename=input_image_file) as img:
        crop_size = min(img.width, img.height)
        img.crop(width=crop_size, height=crop_size, gravity="center")
        img.transform(resize=f"{CONST_X}x{CONST_Y}")

        for i, line in enumerate(text):
            with Drawing() as draw:
                draw.font = CONST_FONT_PATH
                draw.font_size = font_size
                draw.gravity = "north"
                draw.stroke_color = Color("white")
                draw.stroke_width = stroke_width
                draw.fill_color = Color(CONST_FILL_COLOR)
                draw.text(0, start_y + i * SPACING_LINE - STEP_Y * i, line)
                draw(img)

        img.save(filename=output_image_file)


def produce_video(output_image_file: str, input_sound_file: str, output_video_file: str):
    # fmt: off
    run(["ffmpeg",
         "-loglevel", "error",
         "-i", output_image_file,
         "-i", input_sound_file,
         "-c:v", "libvpx-vp9",
         "-c:a", "libopus",
         "-y",
         output_video_file,],
        check=True)
    # fmt: on


def hash_to_levels(obj_hash) -> Tuple[str, str]:
    digest = obj_hash.hexdigest()

    return (digest[0:2], digest[2:])


def check_in_cache(obj_hash) -> bool:
    (level1, level2) = hash_to_levels(obj_hash)

    return Path(f"cache/{level1}/{level2}").exists()


def put_in_cache(obj_hash):
    (level1, level2) = hash_to_levels(obj_hash)

    Path(f"cache/{level1}").mkdir(exist_ok=True)
    with open(f"cache/{level1}/{level2}", "w", encoding="ascii") as _:
        pass


def do_job(data: Tuple[str, str]) -> Optional[str]:
    (letter, phrase) = data

    result_file = phrase.removesuffix(".")
    for char in '\\/:*?"<>|':
        result_file = result_file.replace(char, "")

    output_image_file = f"output/images/{result_file}.jpg"
    output_video_file = f"output/videos/{result_file}.webm"

    try:
        input_image_file = glob(f"input/{letter}/{phrase}/*.jpg")[0]
    except IndexError:
        return f'ERROR: letter="{letter}", phrase="{phrase}" lacks image!'

    image_hash = xxh64()
    image_hash.update(input_image_file)
    with open(input_image_file, "rb") as file_handle:
        image_hash.update(file_handle.read())
    if Path(output_image_file).exists():
        with open(output_image_file, "rb") as file_handle:
            image_hash.update(file_handle.read())

    if not check_in_cache(image_hash):
        draw_image(input_image_file, phrase, output_image_file)

        with open(output_image_file, "rb") as file_handle:
            image_hash.update(file_handle.read())

        put_in_cache(image_hash)

    try:
        input_sound_file = glob(f"input/{letter}/{phrase}/*.{{flac,mp3}}", flags=BRACE)[0]
    except IndexError:
        return f'ERROR: letter="{letter}", phrase="{phrase}" lacks sound!'

    video_hash = xxh64()
    video_hash.update(output_image_file)
    with open(output_image_file, "rb") as file_handle:
        video_hash.update(file_handle.read())
    video_hash.update(input_sound_file)
    with open(input_sound_file, "rb") as file_handle:
        video_hash.update(file_handle.read())
    if Path(output_video_file).exists():
        with open(output_video_file, "rb") as file_handle:
            video_hash.update(file_handle.read())

    if not check_in_cache(video_hash):
        produce_video(output_image_file, input_sound_file, output_video_file)

        with open(output_video_file, "rb") as file_handle:
            video_hash.update(file_handle.read())

        put_in_cache(video_hash)

    return None


def launch():
    if not Path("input").exists():
        print("Please create the `input` folder")
        sysexit(1)

    cpus = len(sched_getaffinity(0))

    jobs = []
    phrases = []
    for letter in iglob("*", root_dir="input"):
        for phrase in iglob("*", root_dir=f"input/{letter}"):
            if phrase not in phrases:
                phrases.append(phrase)
                jobs.append((letter, phrase))
            else:
                print(f'WARNING: phrase="{phrase}" in letter="{letter}" is a duplicate!')

    if len(jobs) == 0:
        print("Please make sure `input` folder is populated")
        sysexit(2)

    Path("cache").mkdir(exist_ok=True)
    Path("output/images").mkdir(parents=True, exist_ok=True)
    Path("output/videos").mkdir(exist_ok=True)

    rets = []
    with Pool(cpus) as pool:
        for ret in tqdm(
            pool.imap_unordered(do_job, jobs),
            total=len(jobs),
            desc="Generating",
            unit="phrase",
            smoothing=0,
        ):
            if ret is not None:
                rets.append(ret)

    for ret in rets:
        print(ret)


if __name__ == "__main__":
    launch()
